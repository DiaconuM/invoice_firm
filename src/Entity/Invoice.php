<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceRepository")
 */
class Invoice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *  @ORM\OneToMany(targetEntity="AppBundle\Entity\Client")
     * @ORM\Column(type="integer")
     */
    private $client;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $tip_factura;

    /**
     *  @ORM\OneToMany(targetEntity="AppBundle\Entity\Client")
     * @ORM\Column(type="integer")
     */
    private $series_item;

    /**
     * @ORM\Column(type="integer")
     */
    private $firm;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firm_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $client_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firm_adress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $client_adress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $client_banca;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firm_banca;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firm_iban;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $client_iban;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?int
    {
        return $this->client;
    }

    public function setClient(int $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getTipFactura(): ?string
    {
        return $this->tip_factura;
    }

    public function setTipFactura(?string $tip_factura): self
    {
        $this->tip_factura = $tip_factura;

        return $this;
    }

    public function getSeriesItem(): ?int
    {
        return $this->series_item;
    }

    public function setSeriesItem(int $series_item): self
    {
        $this->series_item = $series_item;

        return $this;
    }

    public function getFirm(): ?int
    {
        return $this->firm;
    }

    public function setFirm(int $firm): self
    {
        $this->firm = $firm;

        return $this;
    }

    public function getFirmName(): ?string
    {
        return $this->firm_name;
    }

    public function setFirmName(?string $firm_name): self
    {
        $this->firm_name = $firm_name;

        return $this;
    }

    public function getClientName(): ?string
    {
        return $this->client_name;
    }

    public function setClientName(?string $client_name): self
    {
        $this->client_name = $client_name;

        return $this;
    }

    public function getFirmAdress(): ?string
    {
        return $this->firm_adress;
    }

    public function setFirmAdress(?string $firm_adress): self
    {
        $this->firm_adress = $firm_adress;

        return $this;
    }

    public function getClientAdress(): ?string
    {
        return $this->client_adress;
    }

    public function setClientAdress(?string $client_adress): self
    {
        $this->client_adress = $client_adress;

        return $this;
    }

    public function getClientBanca(): ?string
    {
        return $this->client_banca;
    }

    public function setClientBanca(?string $client_banca): self
    {
        $this->client_banca = $client_banca;

        return $this;
    }

    public function getFirmBanca(): ?string
    {
        return $this->firm_banca;
    }

    public function setFirmBanca(?string $firm_banca): self
    {
        $this->firm_banca = $firm_banca;

        return $this;
    }

    public function getFirmIban(): ?string
    {
        return $this->firm_iban;
    }

    public function setFirmIban(?string $firm_iban): self
    {
        $this->firm_iban = $firm_iban;

        return $this;
    }

    public function getClientIban(): ?string
    {
        return $this->client_iban;
    }

    public function setClientIban(?string $client_iban): self
    {
        $this->client_iban = $client_iban;

        return $this;
    }
}
