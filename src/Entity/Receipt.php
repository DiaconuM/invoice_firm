<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReceiptRepository")
 */
class Receipt
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *  @ORM\OneToMany(targetEntity="AppBundle\Entity\Client")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $client;

    /**
     *  @ORM\ManyToOne(targetEntity="AppBundle\Entity\SeriesItem")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $series_item;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $value;

    /**
     *  @ORM\OneToMany(targetEntity="AppBundle\Entity\Firm")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firm;

    /**
     *  @ORM\OneToOne(targetEntity="AppBundle\Entity\Invoice")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $invoice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): ?string
    {
        return $this->client;
    }

    public function setClient(?string $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getSeriesItem(): ?string
    {
        return $this->series_item;
    }

    public function setSeriesItem(?string $series_item): self
    {
        $this->series_item = $series_item;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getFirm(): ?string
    {
        return $this->firm;
    }

    public function setFirm(?string $firm): self
    {
        $this->firm = $firm;

        return $this;
    }

    public function getInvoice(): ?string
    {
        return $this->invoice;
    }

    public function setInvoice(?string $invoice): self
    {
        $this->invoice = $invoice;

        return $this;
    }
}
