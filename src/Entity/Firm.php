<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FirmRepository")
 */
class Firm
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $jud;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $oras;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresa;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cui;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $banca;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cod_iban;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nr_reg_com;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getJud(): ?string
    {
        return $this->jud;
    }

    public function setJud(?string $jud): self
    {
        $this->jud = $jud;

        return $this;
    }

    public function getOras(): ?string
    {
        return $this->oras;
    }

    public function setOras(?string $oras): self
    {
        $this->oras = $oras;

        return $this;
    }

    public function getAdresa(): ?string
    {
        return $this->adresa;
    }

    public function setAdresa(?string $adresa): self
    {
        $this->adresa = $adresa;

        return $this;
    }

    public function getTelefon(): ?string
    {
        return $this->telefon;
    }

    public function setTelefon(?string $telefon): self
    {
        $this->telefon = $telefon;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCui(): ?string
    {
        return $this->cui;
    }

    public function setCui(?string $cui): self
    {
        $this->cui = $cui;

        return $this;
    }

    public function getBanca(): ?string
    {
        return $this->banca;
    }

    public function setBanca(string $banca): self
    {
        $this->banca = $banca;

        return $this;
    }

    public function getCodIban(): ?string
    {
        return $this->cod_iban;
    }

    public function setCodIban(?string $cod_iban): self
    {
        $this->cod_iban = $cod_iban;

        return $this;
    }

    public function getNrRegCom(): ?string
    {
        return $this->nr_reg_com;
    }

    public function setNrRegCom(?string $nr_reg_com): self
    {
        $this->nr_reg_com = $nr_reg_com;

        return $this;
    }
}
