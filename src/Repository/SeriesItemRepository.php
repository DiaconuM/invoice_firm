<?php

namespace App\Repository;

use App\Entity\SeriesItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SeriesItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method SeriesItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method SeriesItem[]    findAll()
 * @method SeriesItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SeriesItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SeriesItem::class);
    }

    // /**
    //  * @return SeriesItem[] Returns an array of SeriesItem objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SeriesItem
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
